### ABOUT THE SITE

This website was built by Buckwheat and Shady utilising the Splide.js script written by Naotoshi Fujita to be a Birthday present to Buckwheat's Dad, who has worked in Information Technology for many decades now. Originally, Buckwheat wanted to write a program as his gift to his Dad, but Shady said "nah bro lemme get this framework and we'll do sum webdev" so we built a website for him instead.

Happy Birthday, Buckwheat's Dad.
