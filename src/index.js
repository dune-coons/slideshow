import Splide from "@splidejs/splide";

new Splide(".splide", {
  width: "100vw",
  height: "100vh",
  direction: "ttb",
  wheel: true,
  cover: true,
  arrows: false,
}).mount();

import corecss from "@splidejs/splide/dist/css/splide-core.min.css";
import themecss from "@splidejs/splide/dist/css/themes/splide-default.min.css";
